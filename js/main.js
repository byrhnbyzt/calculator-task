'use strict'

var screen = document.querySelector(".screen");
var keys = document.querySelectorAll(".btn")
var operators = ['+', '-', 'x', '÷'];
var decimalAdded = false;
let lastOpr = "";
let ifDot = "";

for(let i = 0; i < keys.length; i++){
  keys[i].onclick = function(e){
    var inputValue = screen.value;
    var inputLength = inputValue.length;
    var buttonValue = this.value;

    // Screen limit
    if(inputLength == 8 && buttonValue != "+" && buttonValue != "-" && buttonValue != "x" && buttonValue != "÷"){
      alert("sorry you can't enter more..");
      return false;
    }

    // Clear screen
    if(buttonValue == "C"){
      screen.value = "";
      lastOpr = "";

      decimalAdded = false;
    }

    else if(buttonValue == "="){
      var equation = inputValue;
      equation = equation.replace(/x/g, '*').replace(/÷/g, '/');

      if(equation){
        screen.value = eval(equation);
        screen.value = parseFloat(screen.value).toFixed(1);
        decimalAdded = false;
      }
    }
		
    else if(operators.indexOf(buttonValue) > -1){
      // Get the last character from the equation
      var lastChar = inputValue[inputLength - 1];

      // Only add operator if input is not empty and there is no operator at the last
      if(inputValue != "" && operators.indexOf(lastChar) == -1){
        screen.value += buttonValue;
        lastOpr = buttonValue;
      }

      // Allow minus if the string is empty
      else if(inputValue == "" && buttonValue == "-"){
        screen.value += buttonValue;
        lastOpr = buttonValue;
      }

      // Replace the last operator (if exists) with the newly pressed operator
      if(operators.indexOf(lastChar) > -1 && inputLength > 1){
        screen.value = inputValue.replace(/.$/, buttonValue);
        lastOpr = buttonValue;
      }
      decimalAdded = false;
    }

    // Check if lastOpr = / and button value = 0 then alert
    else if(lastOpr == "÷" && buttonValue == "0"){
      alert("Cannot divide by zero!");
    }

    else if(buttonValue == "." && inputLength == 0){
      if(!decimalAdded){
        screen.value += "0.";
        decimalAdded = true;
      }
    }
    
    else {
      screen.value += buttonValue;
      console.log(inputLength, buttonValue, "clicked");
    }
  }
}